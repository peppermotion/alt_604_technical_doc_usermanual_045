﻿<#
######################################################
| In Daten von MadCap Flare wird nach unerwünschten   |
| Termen gesucht.                                     |
| Anzahl der Fundstellen wird in eine                 |
| Protokolldatei geschrieben                          |
| Script muss im Verzeichnis "../Content/TermCheck"   |
| liegen und durchsucht die Geschwister-Verzeichnisse | 
######################################################
#>

# aktuelles Verzeichnis (=Startpunkt) ermitteln
function Get-ScriptDirectory {
    $Invocation = (Get-Variable MyInvocation -Scope 1).Value
    Split-Path $Invocation.MyCommand.Path
}

$Startpunkt = Get-ScriptDirectory
# Dateiname und Ablageort muss noch definiert werden
#$TermeCSV = "Terme.csv"
$TermeCSV = ".\TermCheck\qryObsolete_Term_de.csv"

cd $Startpunkt
New-Item -Path Protokolle -ItemType directory -ErrorAction SilentlyContinue | Out-Null
New-Item -Path Hilfsdateien -ItemType directory -ErrorAction SilentlyContinue | Out-Null
cd ..

# Datum + Zeit ermitteln (Doppelpunkt kann nicht im Dateinamen verwendet werden)
$Zeit = (Get-Date).ToString(„HH.mm.ss“)
$ZeitP = (Get-Date).ToString(„HH:mm:ss“)
$Datum = (Get-Date).ToString(„yyyy.MM.dd“)
$ZeitDatei = "$Datum($Zeit)"
$ZeitProtokoll = "$Datum($ZeitP)"

# Datei mit Termen laden 
$Terme = import-csv "$TermeCSV" -Header "richtig", "falsch" -Delimiter ";" -encoding UTF8 -ErrorAction SilentlyContinue

# Liste aller css-Daten erstellen und in Variable laden
get-ChildItem *.css -Recurse | Select-Object -Property name,directory | Export-csv -path   .\TermCheck\Hilfsdateien\htm_Dateien.csv -delimiter ";" 

# Liste aller htm-Daten erstellen und in Variable laden
get-ChildItem *.htm -Recurse | Select-Object -Property name,directory | Export-csv -path   .\TermCheck\Hilfsdateien\htm_Dateien.csv -delimiter ";" -Append

# Liste aller props-Daten erstellen und in die HTML-Variable anhängen
get-ChildItem *.props -Recurse | Select-Object -Property name,directory | Export-csv -path   .\TermCheck\Hilfsdateien\htm_Dateien.csv -delimiter ";" -Append
$HTMs = import-csv ".\TermCheck\Hilfsdateien\htm_Dateien.csv" -Header "Datei", "Pfad" -Delimiter ";" -encoding UTF8 -ErrorAction SilentlyContinue

# Protokoll-Datei anlegen und Vorspann schreiben
$Dateiname = $ZeitDatei + "_TerminologiePrüfung.txt"
   # Für Debuggen unten einkommentieren und oben auskommentieren
   # $Dateiname = "TerminologiePrüfung.txt"
#$Startpunkt = $Startpunkt -replace  "\TermCheck", "" 
$Protokoll = "$Startpunkt\Protokolle\$Dateiname" 
$ProtokollText =  "User:  "+ $Env:USERNAME+ "`r`n" +"Datum/Zeit:  "+ $ZeitProtokoll +  "`r`n" + "Verzeichnis:  " + $Startpunkt+  "`r`n"+  "`r`n"
set-content -path $Protokoll  -Value " " -encoding UTF8

#ANFANG Schleife jede Datei durchsuchen
$i=0

foreach ($HTM in $HTMs)
{ 
    $Pfad=$HTM.Pfad
    $Datei=$HTM.Datei
    $PfadDatei = "$Pfad" + "\" + "$Datei"         

    # Inhalt (=$DateiInhalt) der Datei laden
    $DateiInhalt = Get-Content $PfadDatei -encoding UTF8 -ErrorAction SilentlyContinue
    # erste Zeile (Header) ignorieren
    if ($PfadDatei -match "Directory")
     {     
     } 
    else #ANFANG Alle Dateien bearbeiten ausßer Header
    {
    #ANFANG Schleife alle Terme durchlaufen
      # $TZ ist der Zähler der gefunden Terme (verschiedene)
      # $TJ=1 aktueller Term wurde gefunden
      $TZ=0
      foreach ($Term in $Terme)
      {
      $AlleZeilen = ""
      $Zaehler = 0
      $TJ = 0
      $Term_falsch=$Term.falsch
      $Term_richtig=$Term.richtig

      # Zeilenweise einlesen
      foreach ($DateiZeile in $DateiInhalt)
       {     
         # Dateiinhalt in eine Zeile schreiben
		 $AlleZeilen = $AlleZeilen + $DateiZeile
       }
	     # Wortbestandteile werden ausgeschlossen: Vor und nach dem Term dürfen keine Buchstaben sein
         $TPruef = "\W$Term_falsch\W"
         if ($AlleZeilen -match $TPruef)
         {
          $Zaehler++ 
          $TJ=1
          # Anzahl der Vorkommen bestimmen
          $L = $AlleZeilen.Length
          $T = $Term_falsch.Length
          $AlleZeilen = $AlleZeilen -replace $TPruef, "  "
          $LoT = $AlleZeilen.Length
          $Anzahl = ($L - $LoT)/$T 
         }        
       $TZ=$TZ+$TJ
        # Ergebnis der Suche in die Protokoll-Variable schreiben, wenn Zähler >< 0
       if ($Zaehler -ne 0)
        {
         if ($TZ -eq 1)
           {
             # Dateipfad + Name in die Protokoll-Variable schreiben
             $ProtokollText = $ProtokollText + "`r`n" +  $PfadDatei + "`r`n"  
           }      
         # Gefundenen Term in die Protokoll-Variable schreiben
         $ProtokollText =  $ProtokollText + $Anzahl + " X   " + $Term_falsch +"  (" + $Term_richtig + ")`r`n" 
         Write-host Eintrag im Protokoll: $ProtokollText
         }       
      } #ENDE Schleife alle Terme durchlaufen
      
} #ENDE Schleife jede Datei durchsuchen
} #ENDE else - Alle Dateien bearbeiten ausßer Header

# Variableninhalt ins Protokoll schreiben
set-content -path $Protokoll -Value $ProtokollText -encoding UTF8

# Bildschirmausgabe und Pause, damit Fenster offen bleibt
Write-host "Die Datei " $Dateiname "ist im Verzeichnis abgelgt"
pause